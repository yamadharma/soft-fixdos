/*
 * $Id: crlf.cc,v 1.3 2000/02/29 10:40:24 marius Exp $
 */

/*
 * Copyright (C) by Marius van Wyk 2000
 *
 * Misc utils is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
 * Version 2 (June 1991). See the "COPYING" file distributed with this software
 * for more info.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>

#ifndef WIN32
#  include <unistd.h>
#  include <dirent.h>
#  include <limits.h>
#else
#  define strcasecmp(x, y) stricmp(x, y)
#endif

int backup, utod, verbose, ignore, eof;
int c=0, s=0, f=0;

int crlfFile(FILE* in, FILE* out)
{
  int changed=0, error=0;
  int havecr=0;
  int tmp=13;
  int ret;

  for(;!feof(in);)
  {
    c=fgetc(in);

    if(eof && c==0x1a) continue; // ignore all ^Z

    if(c==-1) break;
    if(utod) // unix to dos
    {
      if(c==13) havecr=1;
      else // copy char
      {
        if(c==10 && !havecr)
        {
          ret=fputc(tmp, out);
          changed=1;
          if(ret==-1)
          {
            fprintf(stderr, " error writing to file!\n");
            error=1;
            break;
          }
        }
        havecr=0;
      }
    }
    else     // dos to unix
    {
      if(c==13)
      {
        changed=1;
        continue;
      }
    }
    ret=fputc(c, out);
    if(ret==-1)
    {
      fprintf(stderr, " error writing to output!\n");
      changed=1;
      error=1;
      break;
    }
  }

  // EOF marker
  if(eof && utod)
  {
    changed=1;
    ret=fputc(0x1a, out);
    if(ret==-1)
    {
      fprintf(stderr, " error writing to output!\n");
      error=1;
    }
  }

  return error?-1:changed;
}

int crlf(char* file)
{
#ifndef WIN32
  DIR *dirp;
  int c;
  struct dirent *direntp;
  char path[PATH_MAX];
#endif
  struct stat buf;
  int ret;
  char *tmpName;
  FILE *in, *out;
  int changed=0, error=0;

  int err=stat(file, &buf);
  if(err)
  {
    fprintf(stderr, " error statting \"%s\" (%s)\n",
        file, strerror(errno));
    return -1;
  }
#ifndef WIN32
  if(S_ISDIR(buf.st_mode))
  {
    if(verbose) fprintf(stdout, "Entering directory: %s\n", file);
    dirp=opendir(file);
    if(dirp==NULL)
    {
      fprintf(stderr, "  error (%s) opening \"%s\"\n", strerror(errno), file);
      return -1;
    }

    while ((direntp = readdir(dirp)) != NULL)
    {
      if(strcmp(direntp->d_name, ".") == 0 ||
         strcmp(direntp->d_name, "..") == 0) continue;

      sprintf(path, "%s/%s", file, direntp->d_name);
      ret=crlf(path);
      if(ret==-1) f++;
      else if(ret==0) s++;
      else c++;
    }
    (void)closedir(dirp);
    s--; // Directories don't count;
    return 0;
  }
#else
//  if(buf.st_mode & _S_IFDIR == _S_IFDIR )
//  {
//  }
#endif

  if(verbose) fprintf(stdout, "Processing: %s\n", file);
  tmpName=(char *)malloc(strlen(file)+1+4);
  if(tmpName==NULL)
  {
    fprintf(stderr, " malloc error!\n");
    exit(-1);
  }
  sprintf(tmpName, "%s.bak", file);
  ret=rename(file, tmpName);
  if(ret==-1)
  {
    fprintf(stderr, " error renaming \"%s\" to \"%s\" (%s)\n",
        file, tmpName, strerror(errno));
    free(tmpName);
    return -1;
  }
  in=fopen(tmpName, "rb");
  if(in==NULL)
  {
    fprintf(stderr, " error opening \"%s\" (%s)\n",
        file, strerror(errno));
    free(tmpName);
    return -1;
  }
  out=fopen(file, "w+b");
  if(out==NULL)
  {
    fprintf(stderr, " error opening \"%s\" (%s)\n",
        tmpName, strerror(errno));
    free(tmpName);
    fclose(in);
    return -1;
  }

  switch(crlfFile(in, out))
  {
  case -1:
    error=1;
    break;
  case 0:
    break;
  case 1:
    changed=1;
    break;
  }

  fclose(in);
  fclose(out);
  if((!changed || !backup) && !error)
  {
    unlink(tmpName);
  }

#ifndef WIN32
  if(!error)
  {
    // NOTE: The order of the following may kill the suid bit on files,
    //       but so be it.

    // Fix file permissions.
    err=chmod(file, buf.st_mode);
    if(err==-1)
    {
      fprintf(stderr, "Error (%s) setting permissions on \"%s\"\n",
          strerror(errno), file);
      // Don't flag this as an error.
    }

    // Fix ownership
    err=chown(file, buf.st_uid, buf.st_gid);
    if(err==-1)
    {
      fprintf(stderr, "Error (%s) setting ownership on \"%s\"\n",
          strerror(errno), file);
      // Don't flag this as an error.
    }
  }
#endif

  if(verbose && changed) fprintf(stdout, " changed...\n");
  free(tmpName);
  if(error) return -1;
  return changed;
}

int main(int argc, char *argv[])
{
  int i, ret;
  int stdio = 1;
  c=0; s=0; f=0;

  if(argc==1)
  {
    fprintf(stderr, "Use --help for help\n");
    return -1;
  }

  // options
  verbose=0;
  backup=0;
  utod=0;
  ignore=0;
  eof=1;

  for(i=1;i<argc;i++)
  {
    if(!ignore)
    {
      if(strcmp(argv[i], "--help")==0)
      {
        fprintf(stdout, "Usage: %s <Command>*\n"
            " Commands:\n"
            " --help : print this help and exit\n"
            " -v     : change to verbose mode\n"
            " -q     : change to quiet mode                     - DEFAULT\n"
            " -d     : convert to dos  (cr -> crlf)\n"
            " -u     : convert to unix (crlf -> cr)             - DEFAULT\n"
            " -Z     : ignore DOS EOF marker (^Z) in conversion\n"
            " -z     : check  DOS EOF marker (^Z) in conversion - DEFAULT\n"
            " -b     : create backup of changed files\n"
            " -l     : don't backup changed files               - DEFAULT\n"
            " --     : Ignore further options\n"
            " <filename/directory> : file to change\n"
#ifndef WIN32
            "          & directories to recurse\n"
#endif
            "\nWritten by marius@e.co.za\n" , argv[0]);
        return 0;
      }
      else if(strcmp(argv[i], "-v")==0)
      {
        verbose=1;
        continue;
      }
      else if(strcmp(argv[i], "-q")==0)
      {
        verbose=0;
        continue;
      }
      else if(strcmp(argv[i], "-b")==0)
      {
        backup=1;
        continue;
      }
      else if(strcmp(argv[i], "-l")==0)
      {
        backup=0;
        continue;
      }
      else if(strcmp(argv[i], "-d")==0)
      {
        utod=1;
        continue;
      }
      else if(strcmp(argv[i], "-u")==0)
      {
        utod=0;
        continue;
      }
      else if(strcmp(argv[i], "-z")==0)
      {
        eof=1;
        continue;
      }
      else if(strcmp(argv[i], "-Z")==0)
      {
        eof=0;
        continue;
      }
      else if(strcmp(argv[i], "--")==0)
      {
        ignore=1;
        continue;
      }
    }

    stdio=0;
    ret=crlf(argv[i]);
    if(ret==-1) f++;
    else if(ret==0) s++;
    else c++;
  }

  if(stdio)
  {
    switch(crlfFile(stdin, stdout))
    {
    case -1:
      f=1;
      break;
    case 0:
      s=1;
      break;
    case 1:
      c=1;
      break;
    }
  }

  if(verbose && (c||s||f))
    fprintf(stdout, "Total: %d changed, %d static, %d errors\n", c, s, f);

  return (f?-1:0);
}
