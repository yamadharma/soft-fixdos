### Makefile for fixDos (win32)

# Copyright (C) by Marius van Wyk 2000
#
# Misc utils is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
# Version 2 (June 1991). See the "COPYING" file distributed with this software
# for more info.
#
# Will compile fixDos on Win32 (Windows NT and Windows 95), using the
# Microsoft Visual C++ compilers.
#
# The basic command line to build is:
#	nmake -f make_mvc.mak [<optional targets>]
#
# This will build untab and crlf
#

CFLAGS = /nologo /ML /W3 /O2 /DWIN32 /D_CONSOLE /D_MBCS /FD

all:	untab.exe crlf.exe

untab.exe: untab.c
	$(CC) $(CFLAGS) untab.c

crlf.exe: crlf.c
	$(CC) $(CFLAGS) crlf.c

clean:
	- del *.exe *.obj *.idb
