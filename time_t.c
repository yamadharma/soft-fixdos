/*
 * $Log: time_t.cc,v $
 * Revision 1.2  1999/03/18 15:00:33  marius
 * Added log comments.
 *
 */

#include <time.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int main(argc, argv)
  int argc;
  char *argv[];
{
  time_t t;
  long days, hours, mins, secs;
  if(argc<2 || strcmp(argv[1], "--help")==0)
  {
    fprintf(stdout, "time_t --help | now | <time_t value>\n"
        " --help    : this help\n"
        " now       : outputs the value of time(NULL)\n"
        " <value>   : outputs extened info on the time_t variable\n"
        "             (%d < value < %d)\n" , INT_MIN, INT_MAX);
    return 1;
  }
  else if(strcasecmp(argv[1], "now")==0)
  {
    t=time(NULL);
    fprintf(stdout, "%ld\n", t);
    return 0;
  }

  // else:

  t=atol(argv[1]);
  fprintf(stdout, "Result of ctime(...): %s", ctime(&t));

  days=t/(24L*60L*60L); t=t%(24L*60L*60L);
  hours=t/(60L*60L);    t=t%(60L*60L);
  mins=t/(60L);         t=t%60L;
  secs=t;

  fprintf(stdout, "Duration since epoch: ");
  if(days)  fprintf(stdout, "%ld days ", days);
  if(hours) fprintf(stdout, "%ld hours ", hours);
  if(mins)  fprintf(stdout, "%ld min ", mins);
  if(secs)  fprintf(stdout, "%ld s ", secs);
  if(!t)    fprintf(stdout, " 0 s");
  fprintf(stdout, "\n");
  return 0;
}
