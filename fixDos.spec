Summary: Unix/Dos crlf and filename case conversion utilities.
Name: fixDos
Version: 1.3.1
Release: 1
Copyright: GPL
Group: System Environment/Base
Source: http://e.co.za/marius/downloads/misc/fixDos-1.3.1.tar.gz
Buildroot: /var/tmp/%{name}-root
Packager: Marius van Wyk <marius@e.co.za>

%description
FixDOS contains some utilities: crlf converts files from/to DOS and
UNIX CR/LF file formats, tolower converts filename case to lower/upper
case, untab converts TABs in files to spaces, and time_t returns values
for time handling.

%prep
%setup
# -n fixDos

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make ROOT=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README
/usr/bin/crlf
/usr/bin/time_t
/usr/bin/tolower
/usr/bin/untab

%changelog
* Fri Aug 11 2000 Pieter Nagel <pnagel@tbs.co.za>
- created initial RPM spec file
