/*
 * $Id$
 */

/*
 * Copyright (C) by Marius van Wyk 2000
 *
 * Misc utils is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
 * Version 2 (June 1991). See the "COPYING" file distributed with this software
 * for more info.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>

#ifndef WIN32
#  include <unistd.h>
#  include <dirent.h>
#  include <limits.h>
#else
#  define strcasecmp(x, y) stricmp(x, y)
#endif

int c, s, f;
int nostring, notrail, otab, ntab, backup, verbose, ignore;

enum State
{
  IN_STRING,
  IN_WHITESPACE,
  IN_TEXT
};

int untabFile(FILE* infile, FILE* outfile)
{
  int siz=0;       // Current size of whitespace
  int pos=0;       // Current position on line
  int strchar=0;   // string start char
  int oc=0, c;
  int change=0;
  int copy;
  int line=1;

  int dump=0; // dump spaces

  enum State state = IN_TEXT;

  c=fgetc(infile);
  while(!feof(infile))
  {
    copy=1; // copy char verbatim
    switch(state)
    {
    case IN_TEXT:
      if(c==' ')
      {
        siz++;
        pos++;
        copy=0;
        state=IN_WHITESPACE;
      }
      else if(c=='\t')
      {
        change=1;
        siz+=(otab-pos%otab);
        pos+=(otab-pos%otab);
        copy=0;
        state=IN_WHITESPACE;
      }
      else if(nostring && (c=='\'' || c=='\"'))
      {
        state=IN_STRING;
        strchar = c;
        pos++;
      }
      else if(c=='\n')
      {
        pos=0;
        siz=0;
        line++;
      }
      else
      {
        pos++;
      }
      break;

    case IN_STRING:
      if(c==strchar && oc!='\\')
      {
        state=IN_TEXT;
        strchar = c;
        pos++;
      }
      if(c=='\n')
      {
        pos=0;
        siz=0;
        fprintf(stderr, "  string closing error at line %d (See -S option)\n", line);
        line++;
      }
      break;

    case IN_WHITESPACE:
      if(c==' ')
      {
        siz++;
        pos++;
        copy=0;
      }
      else if(c=='\t')
      {
        change=1;
        siz+=(otab-pos%otab);
        pos+=(otab-pos%otab);
        copy=0;
      }
      else if(c=='\n') // trailing whitespace?
      {
        pos=0;
        if(notrail)
        {
          change=1;
          siz=0;
        }
        else
        {
          dump=1;
        }
        line++;
        state=IN_TEXT;
      }
      else if(nostring && (c=='\'' || c=='\"'))
      {
        strchar=c;
        pos++;
        state=IN_STRING;
        dump=1;
      }
      else
      {
        pos++;
        state=IN_TEXT;
        dump=1;
      }
      break;
    }

    if(dump)
    {
      // No more whitespace
      while(siz--)
      {
        if(fputc(' ', outfile) == EOF) return -1;
      }
      siz=0;

      dump=0;
    }

    if(copy)
    {
      if(fputc(c, outfile) == EOF) return -1;
    }

    oc=c;
    c=fgetc(infile);
  }

  if(state==IN_STRING)
  {
    fprintf(stderr, "  string closing error at EOF (See -S option)\n");
  }

  return change;
}

int untab(const char* file)
{
#ifndef WIN32
  DIR *dirp;
  int c;
  char path[PATH_MAX];
  struct dirent *direntp;
#endif
  struct stat buf;
  int ret;
  char *tmpName;
  FILE *in, *out;
  int changed=0, error=0;

  int err=stat(file, &buf);
  if(err)
  {
    fprintf(stderr, " error statting \"%s\" (%s)\n",
        file, strerror(errno));
    return -1;
  }
#ifndef WIN32
  if(S_ISDIR(buf.st_mode))
  {
    if(verbose) fprintf(stdout, "Entering directory: %s\n", file);
    dirp=opendir(file);
    if(dirp==NULL)
    {
      fprintf(stderr, "  error (%s) opening \"%s\"\n", strerror(errno), file);
      return -1;
    }

    while ((direntp = readdir(dirp)) != NULL)
    {
      if(strcmp(direntp->d_name, ".") == 0 ||
         strcmp(direntp->d_name, "..") == 0) continue;

      sprintf(path, "%s/%s", file, direntp->d_name);
      ret=untab(path);
      if(ret==-1) f++;
      else if(ret==0) s++;
      else c++;
    }
    (void)closedir(dirp);
    s--; // Directories don't count;
    return 0;
  }
#else
//  if(buf.st_mode & _S_IFDIR == _S_IFDIR )
//  {
//  }
#endif

  if(verbose) fprintf(stdout, "Processing: %s\n", file);
  tmpName=(char *)malloc(strlen(file)+1+4);
  if(tmpName==NULL)
  {
    fprintf(stderr, " malloc error!\n");
    exit(-1);
  }
  sprintf(tmpName, "%s.bak", file);
  ret=rename(file, tmpName);
  if(ret==-1)
  {
    fprintf(stderr, " error renaming \"%s\" to \"%s\" (%s)\n",
        file, tmpName, strerror(errno));
    free(tmpName);
    return -1;
  }
  in=fopen(tmpName, "rb");
  if(in==NULL)
  {
    fprintf(stderr, " error opening \"%s\" (%s)\n",
        file, strerror(errno));
    free(tmpName);
    return -1;
  }
  out=fopen(file, "w+b");
  if(out==NULL)
  {
    fprintf(stderr, " error opening \"%s\" (%s)\n",
        tmpName, strerror(errno));
    free(tmpName);
    fclose(in);
    return -1;
  }

  switch(untabFile(in, out))
  {
  case -1: // error
    error=1;
    break;

  case  0: // no error, no change
    break;

  case  1: // no error, change
    changed=1;
    break;
  }

  fclose(in);
  fclose(out);

  if((!changed || !backup) && !error)
  {
    unlink(tmpName);
  }

#ifndef WIN32
  if(!error)
  {
    // NOTE: The order of the following may kill the suid bit on files,
    //       but so be it.

    // Fix file permissions.
    err=chmod(file, buf.st_mode);
    if(err==-1)
    {
      fprintf(stderr, "Error (%s) setting permissions on \"%s\"\n",
          strerror(errno), file);
      // Don't flag this as an error.
    }

    // Fix ownership
    err=chown(file, buf.st_uid, buf.st_gid);
    if(err==-1)
    {
      fprintf(stderr, "Error (%s) setting ownership on \"%s\"\n",
          strerror(errno), file);
      // Don't flag this as an error.
    }
  }
#endif

  if(verbose && changed) fprintf(stdout, " changed...\n");
  free(tmpName);
  if(error) return -1;
  return changed;
}

int main(int argc, char *argv[])
{
  int i, ret;

  // Parse stdio if no files are specified
  int stdio = 1;

  // results
  c=0; s=0; f=0;

  // options
  verbose=0;
  backup=0;
  ignore=0;
  notrail=1;
  nostring=1;
  otab=8;
  ntab=2;

  for(i=1;i<argc;i++)
  {
    if(!ignore)
    {
      if(strcmp(argv[i], "--help")==0)
      {
        fprintf(stderr, "Usage: %s <Command>*\n"
            " <Command>:\n"
            "  --help : print this help and exit\n"
            "  -o <n> : specifiy old hard tab size       - DEFAULT 8\n"
            "  -n <n> : specifiy new soft tab size       - DEFAULT 2\n"
            "  -v     : change to verbose mode\n"
            "  -q     : change to quiet mode             - DEFAULT\n"
            "  -b     : create backup of changed files\n"
            "  -t     : delete trailing whitespace       - DEFAULT\n"
            "  -T     : don't delete trailing whitespace\n"
            "  -s     : keep tabs in C strings           - DEFAULT\n"
            "  -S     : convert tabs in C strings\n"
            "  -l     : don't backup changed files       - DEFAULT\n"
            "  --     : Ignore further options\n"
            "  <filename> : file to change\n"
#ifndef WIN32
            "            & directories to recurse\n"
#endif
            "\nWritten by marius@e.co.za\n" , argv[0]);
        return 0;
      }
      else if(strcmp(argv[i], "-o")==0)
      {
        if(++i < argc)
        {
          otab = atoi(argv[i]);
          if(otab == 0) fprintf(stderr, "-o value posibly invalid (0).\n");
        }
        continue;
      }
      else if(strcmp(argv[i], "-n")==0)
      {
        if(++i < argc)
        {
          ntab = atoi(argv[i]);
          if(ntab == 0) fprintf(stderr, "-n value posibly invalid (0).\n");
        }
        continue;
      }
      else if(strcmp(argv[i], "-v")==0)
      {
        verbose=1;
        continue;
      }
      else if(strcmp(argv[i], "-q")==0)
      {
        verbose=0;
        continue;
      }
      else if(strcmp(argv[i], "-b")==0)
      {
        backup=1;
        continue;
      }
      else if(strcmp(argv[i], "-l")==0)
      {
        backup=0;
        continue;
      }
      else if(strcmp(argv[i], "-t")==0)
      {
        notrail=1;
        continue;
      }
      else if(strcmp(argv[i], "-T")==0)
      {
        notrail=0;
        continue;
      }
      else if(strcmp(argv[i], "-s")==0)
      {
        nostring=1;
        continue;
      }
      else if(strcmp(argv[i], "-S")==0)
      {
        nostring=0;
        continue;
      }
      else if(strcmp(argv[i], "--")==0)
      {
        ignore=1;
        continue;
      }
    }

    stdio = 0;
    ret=untab(argv[i]);
    if(ret==-1) f++;
    else if(ret==0) s++;
    else c++;
  }

  if(stdio)
  {
    // Verbose fine when stream
    switch(untabFile(stdin, stdout))
    {
    case -1: // error
      f=1;
      break;

    case  0: // no error, no change
      s=1;
      break;

    case  1: // no error, change
      c=1;
      break;
    }
  }

  if(verbose && (c||s||f))
    fprintf(stdout, "Total: %d changed, %d static, %d errors\n", c, s, f);

  return (f?-1:0);
}



