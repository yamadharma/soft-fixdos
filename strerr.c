/*
 * $Log: strerr.cc,v $
 * Revision 1.2  1999/03/18 15:00:33  marius
 * Added log comments.
 *
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef __ELASTERROR
#define __ELASTERROR 2000
#endif

int main(argc, argv)
  int argc;
  char *argv[];
{
  int er;
  char *s, *l;
  if(argc<2)
  {
    l=NULL;
    for(er=1;er<=__ELASTERROR;er++)
    {
      s=strerror(er);
      if(s==NULL) continue;
      if(s==l) continue; // a bug - some repetitions on empty
      l=s;
      printf("strerror(%d)\t = \"%s\"\n", er, strerror(er));
    }
    printf("\nstrerr [<errno value>]\n\n");
    return 1;
  }
  er=atol(argv[1]);
  printf("Res: strerror(%d) = %s\n", er, strerror(er));
  return 0;
}
