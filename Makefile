### Makefile for fixDos

# Copyright (C) by Marius van Wyk 2000
#
# Misc utils is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
# Version 2 (June 1991). See the "COPYING" file distributed with this software
# for more info.
#

### Generic settings
TARGETS=crlf tolower untab time_t

INSTALLDIR=$(ROOT)/usr/bin/

# CC=gcc

#################################################
#                                               #
# No user-definable options below this point... #
#                                               #
#################################################

all: $(TARGETS)
	@echo Done making all binaries

crlf:    crlf.o
time_t:  time_t.o
strerr:  strerr.o
tolower: tolower.o

clean: 
	-rm -f core *.o *.a >& /dev/null
	-rm -f $(TARGETS) >& /dev/null
	-rm -f *.exe >& /dev/null
	@echo Removed all temporary files + binaries

install: all
	-mkdir -p $(INSTALLDIR)
	-cp -f $(TARGETS) $(INSTALLDIR)
	@echo Copied all binaries to $(INSTALLDIR)

rpm:
	RPM_BUILD_ROOT=$(HOME)/tmp
	rpm -bb fixDos.spec

.c.o:
	@echo Compiling: $*.c
	$(CC) -c $*.c -o $*.o

.o:
	@echo Linking: $*
	$(CC) $*.o -o $* $(CPPFLAGS)
