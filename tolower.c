/*
 * $Id: tolower.cc,v 1.3 2000/02/29 10:40:24 marius Exp $
 */

/*
 * Copyright (C) by Marius van Wyk 2000
 *
 * Misc utils is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
 * Version 2 (June 1991). See the "COPYING" file distributed with this software
 * for more info.
 *
 */

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

// Option variables
int upper, recurse, verbose, ignore, force, ignMixed;
// char *expr;
// Report variables
int c=0, s=0, f=0;

// Recursion levels
int cur=0;
const int max=666; // Maximum level of recursion

void indent()
{
  int i;
  for(i=0;i<cur;i++) fprintf(stdout, " ");
}

int convert(file)
  char *file;
{
  char path[PATH_MAX];
  DIR *dirp;
  struct dirent *direntp;
  struct stat buf;
  int err;
  long j;
  char *tmpName;
  int isMixed, caseType;

  if(cur>max)
  {
    fprintf(stderr, " error recursing \"%s\" (max level %d)\n",
        file, max);
  }
  else if(recurse)
  {
    err=stat(file, &buf);

    if(err)
    {
      fprintf(stderr, " error statting \"%s\" (%s)\n",
          file, strerror(errno));
      return -1;
    }
    if(S_ISDIR(buf.st_mode))
    {
      if(verbose)
      {
        indent();
        fprintf(stdout, "Entering directory: %s\n", file);
      }
      dirp=opendir(file);
      if(dirp==NULL)
      {
        fprintf(stderr, "  error (%s) opening \"%s\"\n", strerror(errno), file);
        return -1;
      }
      cur++;

      while((direntp=readdir(dirp))!=NULL)
      {
        if(strcmp(direntp->d_name, ".")==0 ||
            strcmp(direntp->d_name, "..")==0) continue;
        sprintf(path, "%s/%s", file, direntp->d_name);
        err=convert(path);
        if(err==-1) f++;
        else if(err==0) s++;
        else c++;
      }
      (void)closedir(dirp);
      cur--;
      // Proceed to rename it anyway
    }
  }

  j=strlen(file);
  if(j>249 || j<1)
  {
    fprintf(stderr, " filename \"%s\" length error!\n", file);
    return -1;
  }
  tmpName=(char *)malloc(j+1);
  if(tmpName==NULL)
  {
    fprintf(stderr, " malloc %ld bytes error!\n", (j+1));
    exit(-1);
  }
  strcpy(tmpName, file);
  isMixed=0;
  caseType=0;
  while(file[j]!='/' && j--)
  {
    if(ignMixed && !isMixed)
    {
      if(tolower(file[j])!=file[j])
      {
        // letter is uppercase
        if(caseType==1) isMixed=1;
        else caseType=2;
      }
      else if(toupper(file[j])!=file[j])
      {
        // letter is lowercase
        if(caseType==2) isMixed=1;
        else caseType=1;
      }
    }
    switch(upper)
    {
    case 0:
      file[j]=tolower(file[j]);
      break;
    case 1:
      file[j]=toupper(file[j]);
      break;
    case 2:
      if(j==0 || file[j-1] == ' ' || file[j-1] == '\t')
        file[j]=toupper(file[j]);
      else
        file[j]=tolower(file[j]);
    }
  }

  if(ignMixed && isMixed)
  {
    if(verbose)
    {
      indent();
      fprintf(stdout, "ignoring mixed case: %s\n", tmpName);
    }
    strcpy(tmpName, file);
  }

  /*
  // Do expression translation
  if(expr!=NULL)
  {
    // regex?
  }
  */

  // Check if source exists
  err=stat(tmpName, &buf);
  if(err)
  {
    fprintf(stderr, " error renaming \"%s\" to \"%s\" (Source does not exist)\n",
        tmpName, file);
    free(tmpName);
    return -1;
  }

  if(strcmp(file, tmpName)==0)
  {
    if(verbose)
    {
      indent();
      fprintf(stdout, "%s unchanged\n", file);
    }
    free(tmpName);
    return 0;
  }

  if(verbose)
  {
    indent();
    fprintf(stdout, "renaming %s -> %s\n", tmpName, file);
  }

  // Check if destination file already exists
  err=stat(file, &buf);
  if(!err)
  {
    if(force)
    {
      if(verbose)
      {
        indent();
        fprintf(stdout, "overwriting existing destination: %s\n", file);
      }
      err=unlink(file);
      if(err==-1)
      {
        fprintf(stderr, " error overwriting target \"%s\"\n (%s)",
            file, strerror(errno));
        free(tmpName);
        return -1;
      }
    }
    else
    {
      fprintf(stderr, " error renaming \"%s\" to \"%s\" (Target exists)\n",
          tmpName, file);
      free(tmpName);
      return -1;
    }
  }

  if(rename(tmpName, file)==-1)
  {
    fprintf(stderr, " error renaming \"%s\" to \"%s\" (%s)\n",
        tmpName, file, strerror(errno));
    free(tmpName);
    return -1;
  }

  // NOTE: The order of the following may kill the suid bit on files,
  //       but so be it.

  // Fix file permissions.
  err=chmod(file, buf.st_mode);
  if(err==-1)
  {
    fprintf(stderr, "Error (%s) setting permissions on \"%s\"\n",
        strerror(errno), file);
    // Don't flag this as an error.
  }

  // Fix ownership
  err=chown(file, buf.st_uid, buf.st_gid);
  if(err==-1)
  {
    fprintf(stderr, "Error (%s) setting ownership on \"%s\"\n",
        strerror(errno), file);
    // Don't flag this as an error.
  }

  free(tmpName);
  return 1;
}

int main(int argc, char *argv[])
{
  int i, ret;
  c=0; s=0; f=0;

  if(argc==1)
  {
    fprintf(stderr, "Use --help for help\n");
    return -1;
  }

  verbose=0;
  recurse=0;
  upper=0;
  ignore=0;
  force=0;
  ignMixed=0;
  // expr=NULL;
  for(i=1;i<argc;i++)
  {
    if(!ignore)
    {
      if(strcmp(argv[i], "--help")==0)
      {
        fprintf(stdout, "Usage: %s <Command>*\n"
            " Commands:\n"
            " --help : Print this help and exit\n"
            " -v     : Change to verbose mode\n"
            " -q     : Change to quiet mode - DEFAULT\n"
            " -u     : Rename files to upper case\n"
            " -l     : Rename files to lower case - DEFAULT\n"
            " -c     : Upper case only start of words\n"
            " -f     : Force overwriting of files.\n"
            " -r     : Recurse directories\n"
//            " -e exp : Do added name mangling using sed expr.\n"
            " -m     : Ignore mixed case filenames\n"
            " -M     : Process even mixed case filenames - DEFAULT\n"
            " --     : Ignore further options\n"
            " <filename/directory> : file to rename &\n"
            "          directory to rename (and recurse using -r)\n\n"
            "Written by marius@e.co.za\n" , argv[0]);
        return 0;
      }
      else if(strcmp(argv[i], "-v")==0)
      {
        verbose=1;
        continue;
      }
      else if(strcmp(argv[i], "-q")==0)
      {
        verbose=0;
        continue;
      }
      else if(strcmp(argv[i], "-l")==0)
      {
        upper=0;
        continue;
      }
      else if(strcmp(argv[i], "-r")==0)
      {
        recurse=1;
        continue;
      }
      else if(strcmp(argv[i], "-m")==0)
      {
        ignMixed=1;
        continue;
      }
      else if(strcmp(argv[i], "-M")==0)
      {
        ignMixed=0;
        continue;
      }
      else if(strcmp(argv[i], "-u")==0)
      {
        upper=1;
        continue;
      }
      else if(strcmp(argv[i], "-c")==0)
      {
        upper=2;
        continue;
      }
      /*
      else if(strcmp(argv[i], "-e")==0)
      {
        i++;
        if(i<argc)
        {
          expr=argv[i];
        }
        else
        {
          fprintf(stderr, "Invalid use of -e option");
        }
        continue;
      }
      */
      else if(strcmp(argv[i], "-f")==0)
      {
        force=1;
        continue;
      }
      else if(strcmp(argv[i], "--")==0)
      {
        ignore=1;
        continue;
      }
    }
    ret=convert(argv[i]);
    if(ret==-1) f++;
    else if(ret==0) s++;
    else c++;
  }

  if(verbose && (c||s||f))
    fprintf(stdout, "Total: %d changed, %d static, %d errors\n",
        c, s, f);
  return (f?-1:0);
}
